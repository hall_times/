#/bin/bash

IFS=$","

declare -a buildings=( $(curl -s https://www.fpm.iastate.edu/maps/buildings/ | awk '/var buildingNames/{ f = 1; next } /];/{ f = 0 } f' | tr -d '"' | tr ' ' '+') )

URL="https://www.fpm.iastate.edu/maps/buildings/building.asp?building="

echo "" > /tmp/building_urls.txt
for ((i = 0; i < ${#buildings[@]}; ++i)) 
do
	printf "$URL${buildings[i]}\n" >> /tmp/building_urls.txt
done

if [ "$1" = "reload" ]; then
	echo "" > /tmp/building_html_time.txt
	parallel curl -s {} < /tmp/building_urls.txt | grep "mt-3\|Monday" >> /tmp/building_html_time.txt 
fi

cat /tmp/building_html_time.txt | grep "mt-3\|tr.*th>" | grep "Monday" -B1 | sed -e 's/--//g' -e 's/<h2 class="mt-3">/\n/g' -e 's/<\/h2>//g' -e 's/<tr><th>//g' -e 's/<\/th><td>/ /g' -e 's/<\/td>/ /g' -e 's/<\/tr>//g' | tee times.txt
